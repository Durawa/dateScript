#!/usr/bin/env bash
fileName=data.txt

if [ "$1" != "" ]; then
    fileName="$1"
fi

echo $(date) > $fileName